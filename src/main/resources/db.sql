DROP TABLE IF EXISTS movie_genre;
DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS movie;
DROP TABLE IF EXISTS director;

-- director
CREATE TABLE director
(
    id            BIGSERIAL PRIMARY KEY NOT NULL,
    first_name    VARCHAR(100)          NOT NULL,
    last_name     VARCHAR(100)          NOT NULL,
    bio           TEXT,
    date_of_birth DATE                  NOT NULL,
    created_at    timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at    timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE director
    OWNER TO hibernate;

-- movie
CREATE TABLE movie
(
    id               BIGSERIAL PRIMARY KEY NOT NULL,
    director_id      BIGINT REFERENCES director (id),
    title            VARCHAR(255)          NOT NULL,
    year             INT                   NOT NULL,
    imdb             FLOAT,
    running_time_min int                   NOT NULL,
    created_at       timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at       timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE movie
    OWNER TO hibernate;

-- genre
CREATE TABLE genre
(
    id   BIGSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(100)          NOT NULL UNIQUE
);

ALTER TABLE genre
    OWNER TO hibernate;

-- movie_genre
CREATE TABLE movie_genre
(
    movie_id BIGINT NOT NULL REFERENCES movie (id),
    genre_id BIGINT NOT NULL REFERENCES genre (id)
);

ALTER TABLE movie_genre
    OWNER TO hibernate;

-- test data
INSERT INTO director (first_name, last_name, bio, date_of_birth)
VALUES ('Emilis',
        'Vėlyvis',
        'Emilis Vėlyvis is a director and writer, known for Redirected (2014), Zero 3 (2017) and Zero 2 (2010).',
        '1979-05-30');

INSERT INTO movie (director_id, title, year, imdb, running_time_min)
VALUES (1, 'Redirected', 2014, 6.6, 99);

INSERT INTO genre (name)
VALUES ('Action'),
       ('Comedy');

INSERT INTO movie_genre (movie_id, genre_id)
VALUES (1, 1),
       (1, 2);
