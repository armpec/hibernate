package lt.codeacademy.services;

import lt.codeacademy.HibernateConfig;
import lt.codeacademy.entities.Genre;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class GenreService {

    public List<Genre> getAll(String key, Object value) {
        return getAll(key, value, false);
    }

    public Genre getById(Long id) {
        List<Genre> genres = getAll("id", id, true);
        return genres.size() > 0 ? genres.get(0) : null;
    }

    public Genre getByName(String name) {
        List<Genre> genres = getAll("name", name, true);
        return genres.size() > 0 ? genres.get(0) : null;
    }

    public List<Genre> getAllByName(String name) {
        return getAll("name", name, false);
    }

    // INSERT OR UPDATE
    public Genre save(Genre genre) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.saveOrUpdate(genre);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return genre;
    }

    public void delete(Genre genre) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.delete(genre);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    public List<Genre> getAll() {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();
        List<Genre> genres = new ArrayList<>();

        try {
            Query<Genre> query = session.createQuery("FROM Genre", Genre.class);
            genres = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return genres;
    }

    private List<Genre> getAll(String key, Object value, boolean limitOne) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();
        List<Genre> genres = new ArrayList<>();

        try {
            Query<Genre> query = session.createQuery(String.format("FROM Genre WHERE %s = :%s", key, key), Genre.class);
            query.setParameter(key, value);

            if (limitOne) {
                query.setMaxResults(1);
            }

            genres = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return genres;
    }
}
