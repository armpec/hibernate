package lt.codeacademy.services;

import lt.codeacademy.HibernateConfig;
import lt.codeacademy.entities.Movie;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class MovieService {

    public List<Movie> getAll(String key, Object value) {
        return getAll(key, value, false);
    }

    public Movie getById(Long id) {
        List<Movie> movies = getAll("id", id, true);
        return movies.size() > 0 ? movies.get(0) : null;
    }

    public Movie getByTitle(String title) {
        List<Movie> movies = getAll("title", title, true);
        return movies.size() > 0 ? movies.get(0) : null;
    }

    public List<Movie> getAllByTitle(String title) {
        return getAll("title", title, false);
    }

    // INSERT OR UPDATE
    public Movie save(Movie movie) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.saveOrUpdate(movie);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return movie;
    }

    public void delete(Movie movie) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.delete(movie);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    // HQL example
    public List<Movie> getAll() {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();
        List<Movie> movies = new ArrayList<>();

        try {
            Query<Movie> query = session.createQuery("FROM Movie", Movie.class);
            movies = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return movies;
    }

    // Criteria Queries example
    public List<Movie> getAllByYearGreaterThanOrEqual(Integer year) {
        Session session = HibernateConfig.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteriaQuery.from(Movie.class);

        criteriaQuery.select(root).where(criteriaBuilder.greaterThanOrEqualTo(root.get("year"), year));
        Query<Movie> query = session.createQuery(criteriaQuery);

        return getAll(session, query, false);
    }

    // Criteria Queries example
    public List<Movie> getAllByYearLessThanOrEqual(Integer year) {
        Session session = HibernateConfig.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteriaQuery.from(Movie.class);

        criteriaQuery.select(root).where(criteriaBuilder.lessThanOrEqualTo(root.get("year"), year));
        Query<Movie> query = session.createQuery(criteriaQuery);

        return getAll(session, query, false);
    }

    // Criteria Queries example
    public List<Movie> getByYearLessThanOrEqual(Integer year) {
        Session session = HibernateConfig.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteriaQuery.from(Movie.class);

        criteriaQuery.select(root).where(criteriaBuilder.lessThanOrEqualTo(root.get("year"), year));
        Query<Movie> query = session.createQuery(criteriaQuery);

        return getAll(session, query, true);
    }

    // Criteria Queries example
    public List<Movie> getAllByTitleLike(String title) {
        Session session = HibernateConfig.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteriaQuery.from(Movie.class);

        criteriaQuery.select(root).where(criteriaBuilder.like(root.get("title"), title));
        Query<Movie> query = session.createQuery(criteriaQuery);

        return getAll(session, query, false);
    }

    // Criteria Queries example
    public List<Movie> getAll(Session session, Query<Movie> query, boolean limitOne) {
        Transaction transaction = session.beginTransaction();
        List<Movie> movies = new ArrayList<>();

        try {
            if (limitOne) {
                query.setMaxResults(1);
            }

            movies = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return movies;
    }

    // HQL example
    private List<Movie> getAll(String col, Object val, boolean limitOne) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();
        List<Movie> movies = new ArrayList<>();

        try {
            Query<Movie> query = session.createQuery(String.format("FROM Movie WHERE %s = :%s", col, col), Movie.class);
            query.setParameter(col, val);

            if (limitOne) {
                query.setMaxResults(1);
            }

            movies = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return movies;
    }
}
