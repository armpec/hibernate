package lt.codeacademy.services;

import lt.codeacademy.HibernateConfig;
import lt.codeacademy.entities.Director;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class DirectorService {

    public List<Director> getAll(String key, Object value) {
        return getAll(key, value, false);
    }

    public Director getById(Long id) {
        List<Director> directors = getAll("id", id, true);
        return directors.size() > 0 ? directors.get(0) : null;
    }

    public Director getByFirstName(String firstName) {
        List<Director> directors = getAll("firstName", firstName, true);
        return directors.size() > 0 ? directors.get(0) : null;
    }

    public List<Director> getAllByFirstName(String firstName) {
        return getAll("firstName", firstName, false);
    }

    public Director getByLastName(String lastName) {
        List<Director> directors = getAll("lastName", lastName, true);
        return directors.size() > 0 ? directors.get(0) : null;
    }

    public List<Director> getAllByLastName(String lastName) {
        return getAll("lastName", lastName, false);
    }

    // INSERT OR UPDATE
    public Director save(Director director) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.saveOrUpdate(director);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return director;
    }

    public void delete(Director director) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.delete(director);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }
    }

    private List<Director> getAll(String key, Object value, boolean limitOne) {
        Session session = HibernateConfig.openSession();
        Transaction transaction = session.beginTransaction();
        List<Director> directors = new ArrayList<>();

        try {
            Query<Director> query = session.createQuery(String.format("FROM Director WHERE %s = :%s", key, key), Director.class);
            query.setParameter(key, value);

            if (limitOne) {
                query.setMaxResults(1);
            }

            directors = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

        return directors;
    }
}
