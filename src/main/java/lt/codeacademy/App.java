package lt.codeacademy;

import lt.codeacademy.entities.Movie;
import lt.codeacademy.services.DirectorService;
import lt.codeacademy.services.GenreService;
import lt.codeacademy.services.MovieService;

import java.util.List;

public class App {

    public static void main(String[] args) {
        HibernateConfig.buildSessionFactory();

        MovieService movieService = new MovieService();
        GenreService genreService = new GenreService();
        DirectorService directorService = new DirectorService();

        List<Movie> movies = movieService.getAllByTitleLike("%test%");

        HibernateConfig.closeSessionFactory();
    }
}
