package lt.codeacademy.services;

import lt.codeacademy.HibernateConfig;
import lt.codeacademy.entities.Director;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class DirectorServiceTest {

    private final DirectorService directorService = new DirectorService();

    private final List<Director> testDirectors = new ArrayList<>();

    @BeforeAll
    static void beforeAll() {
        HibernateConfig.buildSessionFactory();
    }

    @AfterAll
    static void afterAll() {
        HibernateConfig.closeSessionFactory();
    }

    @BeforeEach
    void beforeEach() {
        for (int i = 0; i < 10; i++) {
            Director director = new Director("TestFirstName" + i, "TestLastName" + i, "...", LocalDate.of(2000, 1, 1));
            directorService.save(director);
            Assertions.assertEquals(director.getId(), directorService.getById(director.getId()).getId());
            testDirectors.add(director);
        }
    }

    @AfterEach
    void afterEach() {
        testDirectors.forEach(testDirector -> {
            directorService.delete(testDirector);
            Assertions.assertNull(directorService.getById(testDirector.getId()));
        });
    }

    @Test
    void getByFirstName() {
        testDirectors.forEach(testDirector -> Assertions.assertEquals(testDirector.getFirstName(),
                        directorService.getByFirstName(testDirector.getFirstName()).getFirstName()));
        Assertions.assertNull(directorService.getByFirstName(UUID.randomUUID().toString()));
    }
}
